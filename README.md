# Curso Ciencia de Datos Geoespaciales con Python

Bienvenidos al repositorio del curso.

# Módulos del curso

## 01 Fundamentos de Ciencias de datos con Python
Abordaremos los conceptos de nivel intermedio de programación de computadores así como la recolección, visualización y  tratamiento de datos geoespaciales.

## 02 Proceso de análisis de datos 
Exploraremos la analítica de datos como un proceso cíclico de descripción, exploración, diagnóstico y prescripción de los datos que un dominio produce.

## 03 Ciencias de datos espaciales. 
Tomando como partida los resultados del análisis de datos, propondremos soluciones a un conjunto de casos de estudio, interpretando los hechos y estableciendo causalidades para proponer escenarios prospectivos. Profundizaremos en la aplicación de la teoría del aprendizaje estadístico y realizaremos estudios especializados de regresión, clasificación, análisis de conglomerados, entre otras.

## 0 Presentación de resultados
El conocimiento de un dominio así como las predicciones que se puedan realizar sobre su dinámica futura, deben ser correctamente socializados para que los interesados puedan dirigir la acción. Estudiaremos los elementos para distribuir nuestros hallazgos.
